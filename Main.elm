module Main exposing (Model, Msg, update, view, subscriptions, init)

import Html exposing (..)
import Time exposing (every, Time, millisecond)
import Player exposing (onTick, new, Player, onKeysPressedUpdate)
import Keyboard exposing (downs, ups, KeyCode)
import Set exposing (Set, member, remove, insert, empty)
import Color
import AnimationFrame
import Game.TwoD.Camera as Camera exposing (Camera)
import Game.TwoD.Render as Render exposing (Renderable, circle)
import Game.TwoD as Game
import Config exposing (fieldWidth, fieldHeight)


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { tick : Int
    , player1 : Player
    , keysPressed : Set KeyCode
    }


type Msg
    = Tick Time
    | KeyDown KeyCode
    | KeyUp KeyCode


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick t ->
            ( { model | tick = model.tick + 1, player1 = Player.onTick model.player1 }, Cmd.none )

        KeyDown code ->
            let
                newKeysPressed =
                    insert code model.keysPressed

                newPlayer1 =
                    Player.onKeysPressedUpdate newKeysPressed model.player1
            in
                ( { model | keysPressed = newKeysPressed, player1 = newPlayer1 }, Cmd.none )

        KeyUp code ->
            let
                newKeysPressed =
                    remove code model.keysPressed

                newPlayer1 =
                    Player.onKeysPressedUpdate newKeysPressed model.player1
            in
                ( { model | keysPressed = newKeysPressed, player1 = newPlayer1 }, Cmd.none )


view : Model -> Html Msg
view model =
    div []
        [ text "clock: "
        , text (toString model.tick)
        , div [] [ text "player1 pos: ", text "x: ", text (toString model.player1.pos.x), text " y: ", text (toString model.player1.pos.y) ]
        , Game.renderCentered { time = 0, camera = Camera.fixedArea (fieldWidth * fieldHeight) ( 0, 1.5 ), size = ( round fieldWidth, round fieldHeight ) }
            [ Render.shape circle { color = Color.blue, position = ( model.player1.pos.x, model.player1.pos.y ), size = ( model.player1.size.width, model.player1.size.height ) }
            ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ downs KeyDown, ups KeyUp, AnimationFrame.diffs Tick ]


init : ( Model, Cmd Msg )
init =
    ( { tick = 0, player1 = Player.new { x = 0, y = 0 } { down = 40, up = 38, left = 37, right = 39 }, keysPressed = empty }, Cmd.none )
