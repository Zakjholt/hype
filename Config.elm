module Config exposing (fieldWidth, fieldHeight)


fieldWidth : Float
fieldWidth =
    800


fieldHeight : Float
fieldHeight =
    600
