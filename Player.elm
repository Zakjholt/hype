module Player exposing (onTick, new, Player, onKeysPressedUpdate)

import Set exposing (Set, member, insert, remove)
import Keyboard exposing (KeyCode)
import Config exposing (fieldWidth, fieldHeight)


type alias Position =
    { x : Float
    , y : Float
    }


type alias Velocity =
    { x : Float
    , y : Float
    }


type alias Controls =
    { down : KeyCode
    , up : KeyCode
    , left : KeyCode
    , right : KeyCode
    }


type alias Size =
    { width : Float
    , height : Float
    }


type alias Player =
    { pos : Position
    , vel : Velocity
    , controls : Controls
    , size : Size
    }


onTick : Player -> Player
onTick player =
    let
        leftMost = (fieldWidth / 2) * -1
        rightMost = (fieldWidth / 2) - player.size.width
        newX =
            if (player.pos.x + player.vel.x) <= leftMost then
                leftMost
            else if (player.pos.x + player.vel.x) >= rightMost then
                rightMost
            else
                player.pos.x + player.vel.x
        topMost = (fieldHeight / 2) - player.size.height
        botMost = (fieldHeight / 2) * -1
        newY =
            if (player.pos.y + player.vel.y) <= botMost then
                botMost
            else if (player.pos.y + player.vel.y) >= topMost then
                topMost
            else
                player.pos.y + player.vel.y
    in
        { player | pos = { x = newX, y = newY } }


new : Position -> Controls -> Player
new pos con =
    { pos = pos, vel = { x = 0, y = 0 }, controls = con, size = { width = 50, height = 50 } }


onKeysPressedUpdate : Set KeyCode -> Player -> Player
onKeysPressedUpdate newKeys player =
    let
        y =
            if member player.controls.up newKeys && not (member player.controls.down newKeys) then
                5
            else if member player.controls.down newKeys && not (member player.controls.up newKeys) then
                -5
            else
                0

        x =
            if member player.controls.left newKeys && not (member player.controls.right newKeys) then
                -5
            else if member player.controls.right newKeys && not (member player.controls.left newKeys) then
                5
            else
                0
    in
        { player | vel = { x = x, y = y } }
